# 23CreativeTest For Niavo

This is a test about graphql and react using hooks .

## Getting started

There are two separate branches for backend(origin/backend) and frontend(origin/frontend).

Simply merge the two features into the main branches to synchronize  utilization.

## BackEnd
`cd frontend`
### Install all packages needed
`npm install`

### Serving application
`npm start`

## FrontEnd
`cd backend`

### Install all packages needed
`npm install`

### Serving application
`npm start`

## Facultative process
`take database into backend named "test23creaniavo.sql"` and dump it into the database existant

# Test Goal
## onboardingtest
## fullstack*js*test
Le but de ce test est de tester la compétence du candidat en developpement fullstack web javascript.

### Sujet
Creer une application CRUD employé web avec un API Graphql Nodejs, ReactJS. 
* Employé:``nom, prenom, age, poste, experience``
* Experience: ``titre, description``
* 
### Spec technique

* API: GrahqlJS, Mongodb ou Mysql, Expressjs, ES6
* Web: Apollo client, ReactJS, Apollo Client
* Repository: Gitlab
* Workflow: Utiliser le système de branch

### Indication
Ne vous inquiétez pas si vous ne pouvez pas tout faire, faites de votre mieux :). Nous préférons de petits changements très propres et utiles plutôt que beaucoup de changements inachevés
